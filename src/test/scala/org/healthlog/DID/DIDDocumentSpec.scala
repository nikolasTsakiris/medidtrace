package org.healthlog.DID

import io.circe.Json
import io.circe.syntax._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class DIDDocumentSpec extends AnyFlatSpec with Matchers {

  "DIDDocument" should "generate a valid DIDDocument for a patient" in {
    val didDocument = DIDDocument.generateDIDDocument("patient-public-key")

    didDocument.id should startWith("DID:healthID:")
    //didDocument.verificationMethod.head.`type` shouldBe "Ed25519VerificationKey2018"
    //didDocument.authentication.head.`type` shouldBe "Ed25519SignatureAuthentication2018"
    didDocument.controller.head.`type` shouldBe "GovernmentOrganization"
    didDocument.serviceEndpoints.length shouldBe 3

    val json: Json = didDocument.asJson
    val parsedDocument = json.as[DIDDocument]

    parsedDocument shouldBe Right(didDocument)
  }

}
