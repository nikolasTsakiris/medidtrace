package org.healthlog.key

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import tsec.cipher.common.padding.NoPadding
import tsec.cipher.symmetric._
import tsec.cipher.symmetric.jca._
import tsec.cipher.symmetric.jca.primitive.JCAPrimitiveCipher

class SymmetricSpec extends AnyFlatSpec with Matchers {

  private val testMessage = "This is a test message"

  implicit private val ctrStrategy: IvGen[IO, AES256CTR] = AES256CTR.defaultIvStrategy[IO]
  implicit private val cachedInstance: JCAPrimitiveCipher[IO, AES256CTR, CTR, NoPadding] = AES256CTR.genEncryptor[IO]

  "Symmetric" should "encrypt and decrypt messages using AES256CTR" in {

    val key: SecretKey[AES256CTR] = AES256CTR.unsafeGenerateKey

    val encryptedIO = Symmetric.symEncrypt(testMessage, key.getEncoded)
    val decryptedIO = encryptedIO.flatMap(ciphertext => Symmetric.symDecrypt(ciphertext, key.getEncoded))

    val decryptedMessage = decryptedIO.unsafeRunSync()

    decryptedMessage shouldBe testMessage
  }

}
