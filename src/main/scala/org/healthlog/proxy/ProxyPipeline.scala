package org.healthlog.proxy

import cats.effect.IO
import com.ironcorelabs.recrypt.{CoreApi, EncryptedValue, Plaintext, PrivateKey, PrivateSigningKey, PublicKey, PublicSigningKey, TransformKey}
import org.healthlog.signature.SignaturePipeline

object ProxyPipeline {

  /**
   * Perform a series of operations to proxy re-encrypt a message for a receiver using the given CoreApi instance.
   *
   * @param coreApi           An instance of CoreApi for performing encryption, transformation, and other cryptographic operations.
   * @param receiverPublicKey The public key of the receiver who will decrypt the transformed message.
   * @return An IO containing the re-encrypted message as an EncryptedValue.
   */
  def proxyJobs(
                 coreApi: CoreApi,
                 symmetric_key: Plaintext,
                 senderPrivateKey: PrivateKey,
                 senderPublicKey: PublicKey,
                 receiverPublicKey: PublicKey
               ): IO[EncryptedValue] = {

    //TODO move SIGNING to separate module
    val PUSK: PublicSigningKey = SignaturePipeline.senderPublicSigningKey
    val PRSK: PrivateSigningKey = SignaturePipeline.senderPrivateSigningKey

    val encryptedValue: IO[EncryptedValue] = for {
      encryptedValue <- coreApi.encrypt(symmetric_key, senderPublicKey, PUSK, PRSK)
    } yield encryptedValue

    val transformKey: IO[TransformKey] = for {
      transformKey <- coreApi.generateTransformKey(senderPrivateKey, receiverPublicKey, PUSK, PRSK)
    } yield transformKey

    val transformedValue: IO[EncryptedValue] = for {
      encryptedValue <- encryptedValue
      transformKey <- transformKey
      transformedValue <- coreApi.transform(encryptedValue, transformKey, PUSK, PRSK)
    } yield transformedValue

    transformedValue
  }
}