package org.healthlog.cassandra

import com.datastax.oss.driver.api.core.{CqlSession, DriverException, NoNodeAvailableException}
import com.datastax.oss.driver.api.core.cql.{BoundStatement, PreparedStatement, SimpleStatement}
import com.datastax.oss.driver.api.core.servererrors.QueryValidationException
import com.typesafe.config.ConfigFactory
import io.circe.Json
import io.circe.syntax.EncoderOps
import org.healthlog.DID.DIDDocument.keyAgreementMethodDecoder
import org.healthlog.DID._

import java.net.InetSocketAddress
import java.time.Instant
import java.util.UUID

private object CassandraConfig {
  private val config = ConfigFactory.load()

  val cassandraHost: String = config.getString("cassandra.host")
  val cassandraPort: Int = config.getInt("cassandra.port")
  val cassandraDatacenter: String = config.getString("cassandra.datacenter")
}

object DIDRegistry {

  private val cassandraHost = new InetSocketAddress(
    CassandraConfig.cassandraHost,
    CassandraConfig.cassandraPort
  )
  private val localDatacenter = CassandraConfig.cassandraDatacenter

  /**
   * Adds a Decentralized Identifier (DID) and associated information to the DID Registry.
   *
   * @param subject The holder representing the entity for which the DID is being added.
   * @return Unit
   */
  def addDID(subject: Holder, didType: String): Unit = {

    val keyspaceName = "decentralized_identity_keyspace"
    val insertDidCql = s"INSERT INTO $keyspaceName.did_registry (" +
      s" did," +
      s" type," +
      s" public_key," +
      s" document," +
      s" created_at" +
      s") VALUES (?, ?, ?, ?, ?);"
    val session = CqlSession.builder().addContactPoint(cassandraHost).withLocalDatacenter(localDatacenter).build()

    try {
      val preparedStatement: PreparedStatement = session.prepare(insertDidCql)

      val retrievedDID: Option[DIDDocument] = Holder.requestDID(subject)
      val json: Json = retrievedDID.fold(Json.Null)(_.asJson)
      val keyAgreement: Seq[KeyAgreement] = json.hcursor.downField("keyAgreement").as[Seq[KeyAgreement]].getOrElse(Seq.empty)

      val did: String = json.hcursor.downField("id").as[String].getOrElse("")
      val publicKeyMultibaseValues: Seq[String] = keyAgreement.map(_.publicKeyMultibase)
      val document: String = json.noSpaces
      val createdAt: Instant = Instant.now()

      val boundStatement: BoundStatement = preparedStatement.bind(
        did,
        didType,
        publicKeyMultibaseValues.mkString(","),
        document,
        createdAt
      )
      session.execute(boundStatement)
      println(s"DID $did inserted into $keyspaceName.did_registry successfully.")
    } finally {

      session.close()
    }
  }


  /**
   * Adds data related to a corresponding Decentralized Identifier (DID) Document to the Healthcare Data KeySpace.
   *
   * @param subject The holder representing the entity for which the data is being added.
   * @return Unit
   */
  def addHealthcareData(subject: Holder): Unit = {

    val keyspaceName = "healthcare_data_keyspace"
    val insertHealthcareDataCql = s"INSERT INTO $keyspaceName.healthcare_data (" +
      s" did," +
      s" record_id," +
      s" last_updated_at" +
      s") VALUES (?, ?, ?);"
    val session = CqlSession.builder().addContactPoint(cassandraHost).withLocalDatacenter(localDatacenter).build()

    try {
      val healthcareDataPreparedStatement: PreparedStatement = session.prepare(insertHealthcareDataCql)

      val retrievedDID: Option[DIDDocument] = Holder.requestDID(subject)
      val json: Json = retrievedDID.fold(Json.Null)(_.asJson)

      val did: String = json.hcursor.downField("id").as[String].getOrElse("")
      val recordId: UUID = UUID.randomUUID()
      val lastUpdatedAt: Instant = Instant.now()

      val healthcareDataBoundStatement: BoundStatement = healthcareDataPreparedStatement.bind(
        did,
        recordId,
        lastUpdatedAt
      )
      session.execute(healthcareDataBoundStatement)
      println(s"Healthcare data inserted for user $did and record $recordId into $keyspaceName.healthcare_data successfully.")
    } finally {

      session.close()
    }
  }

  /**
   * Adds an access control policy to the access control keyspace.
   *
   * @param delegator The holder granting access.
   * @param delegatee The holder being granted access.
   */
  def addAccessControl(delegator: Holder, delegatee: Holder): Unit = {

    val keyspaceName = "access_control_keyspace"
    val insertHealthcareDataCql = s"INSERT INTO $keyspaceName.access_control_policies (" +
      s" did," +
      s" allowed_users," +
      s" last_updated_at" +
      s") VALUES (?, ?, ?);"
    val session = CqlSession.builder().addContactPoint(cassandraHost).withLocalDatacenter(localDatacenter).build()

    try {
      val healthcareDataPreparedStatement: PreparedStatement = session.prepare(insertHealthcareDataCql)

      val delegatorRetrievedDID: Option[DIDDocument] = Holder.requestDID(delegator)
      val delegatorJson: Json = delegatorRetrievedDID.fold(Json.Null)(_.asJson)
      val delegateeDID: Option[DIDDocument] = Holder.requestDID(delegatee)
      val delegateeJson: Json = delegateeDID.fold(Json.Null)(_.asJson)

      val delegatorDid: String = delegatorJson.hcursor.downField("id").as[String].getOrElse("")
      val delegateeDid: String = delegateeJson.hcursor.downField("id").as[String].getOrElse("")
      val allowedUser: String = delegateeDid
      val lastUpdatedAt: Instant = Instant.now()

      val healthcareDataBoundStatement: BoundStatement = healthcareDataPreparedStatement.bind(
        delegatorDid,
        allowedUser,
        lastUpdatedAt
      )
      session.execute(healthcareDataBoundStatement)
      println(s"User $allowedUser has access on data of user $delegatorDid")
    } finally {

      session.close()
    }
  }

  /**
   * Adds an audit trail entry to the audit trails key space.
   *
   * @param source     The holder representing the source entity of the action.
   * @param target     The holder representing the target entity of the action.
   * @param actionType The type of action being audited.
   * @return Unit
   */
  def addAuditTrail(source: Holder, target: Holder, actionType: String): Unit = {
    val keyspaceName = "audit_trail_keyspace"
    val insertAuditTrailCql =
      s"INSERT INTO $keyspaceName.audit_trails (" +
        s" FROM_did," +
        s" TO_did," +
        s" action_type," +
        s" timestamp" +
        s") VALUES (?, ?, ?, ?);"
    val session = CqlSession.builder().addContactPoint(cassandraHost).withLocalDatacenter(localDatacenter).build()

    try {
      val auditTrailPreparedStatement: PreparedStatement = session.prepare(insertAuditTrailCql)

      val sourceDoc: Option[DIDDocument] = Holder.requestDID(source)
      val sourceJson: Json = sourceDoc.fold(Json.Null)(_.asJson)
      val targetDoc: Option[DIDDocument] = Holder.requestDID(target)
      val targetJson: Json = targetDoc.fold(Json.Null)(_.asJson)

      val sourceDid: String = sourceJson.hcursor.downField("id").as[String].getOrElse("")
      val targetDid: String = targetJson.hcursor.downField("id").as[String].getOrElse("")
      val timestamp: Instant = Instant.now()

      val auditTrailBoundStatement: BoundStatement = auditTrailPreparedStatement.bind(
        sourceDid,
        targetDid,
        actionType,
        timestamp
      )
      session.execute(auditTrailBoundStatement)
      println(s"Audit trail recorded: $actionType from $sourceDid to $targetDid at $timestamp")
    } finally {
      session.close()
    }
  }

  /**
   * Truncate all tables in the Cassandra keyspaces related to the application.
   * This operation removes all data from the specified tables.
   */
  def truncateAll(): Unit = {
    val ksp1 = "decentralized_identity_keyspace"
    val ksp2 = "healthcare_data_keyspace"
    val ksp3 = "access_control_keyspace"
    val ksp4 = "audit_trail_keyspace"
    val session = CqlSession.builder().addContactPoint(cassandraHost).withLocalDatacenter(localDatacenter).build()

    try {
      val truncateStatement1: SimpleStatement = SimpleStatement.newInstance(s"TRUNCATE $ksp1.did_registry;")
      session.execute(truncateStatement1)

      val truncateStatement2: SimpleStatement = SimpleStatement.newInstance(s"TRUNCATE $ksp2.healthcare_data;")
      session.execute(truncateStatement2)

      val truncateStatement3: SimpleStatement = SimpleStatement.newInstance(s"TRUNCATE $ksp3.access_control_policies;")
      session.execute(truncateStatement3)

      val truncateStatement4: SimpleStatement = SimpleStatement.newInstance(s"TRUNCATE $ksp4.audit_trails;")
      session.execute(truncateStatement4)

      println("All tables truncated successfully.")
    } catch {
      case ex: Exception =>
        println(s"Unexpected error: ${ex.getMessage}")
    } finally {
      session.close()
    }
  }
}