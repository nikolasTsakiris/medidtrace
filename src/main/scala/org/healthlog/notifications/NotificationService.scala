package org.healthlog.notifications

import cats.effect.IO

// noinspection ScalaWeakerAccess
object NotificationService {

  //TODO FIX-EM-ALL
  /**
   * Send a notification to a recipient.
   *
   * @param message   The notification message to send.
   * @param recipient The recipient of the notification.
   * @return An `IO` effect representing the notification action.
   */
  def sendNotification(message: String, recipient: String): Unit = {
    IO(println(s"Sending notification to $recipient: $message"))
  }

  /**
   * Notify a patient that their DID was accessed by a doctor.
   *
   * @param patientDID       The ID of patient's DID.
   * @param accessingParty The name of the Doctor that accessed the DID.
   * @return An `IO` effect representing the notification action.
   */
  def notifyDoctorAccessedDID(patientDID: String, accessingParty: String): Unit = {
    val notificationMessage = s"Your DID $patientDID was accessed by: $accessingParty"
    val recipient = "Doctor"
    sendNotification(notificationMessage, recipient)
  }

  /**
   * Notify a government agency that a patient's DID was accessed by a healthcare service.
   *
   * @param patientDID       The ID of patient's DID.
   * @param accessingParty The name of the healthcare service that accessed the DID.
   * @return An `IO` effect representing the notification action.
   */
  def notifyHealthcareServiceAccessedDID(patientDID: String, accessingParty: String): Unit = {
    val notificationMessage = s"Healthcare service $accessingParty accessed patient's DID: $patientDID"
    val recipient = "HealthcareService"
    sendNotification(notificationMessage, recipient)
  }

  /**
   * Handle the notification based on the role of the recipient.
   *
   * @param patientDID       The ID of patient's DID.
   * @param role             The role of the recipient (e.g., "Patient," "GovernmentAgency").
   * @return An `IO` effect representing the notification action.
   */
  def handleNotification(patientDID: String, role: String): Unit = {
    role match {
      case "Doctor" => notifyDoctorAccessedDID(patientDID, role)
      case "HealthcareService" => notifyHealthcareServiceAccessedDID(patientDID, role)
    }
  }
}