package org.healthlog.signature

import org.bouncycastle.crypto.Digest
import org.bouncycastle.crypto.digests.SHA256Digest
import scodec.bits.ByteVector

object SHA256 {
  def sha256Hash(input: ByteVector): ByteVector = {
    val sha256Digest: Digest = new SHA256Digest()
    val output = new Array[Byte](sha256Digest.getDigestSize)

    sha256Digest.update(input.toArray, 0, input.length.toInt)
    sha256Digest.doFinal(output, 0)

    ByteVector(output)
  }
}

