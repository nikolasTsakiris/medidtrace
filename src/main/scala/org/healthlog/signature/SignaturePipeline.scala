package org.healthlog.signature

import cats.effect.unsafe.implicits.global
import com.ironcorelabs.recrypt.internal.Signature
import com.ironcorelabs.recrypt.{Ed25519Signing, PrivateSigningKey, PublicSigningKey}
import org.bouncycastle.crypto.AsymmetricCipherKeyPair
import org.bouncycastle.crypto.params.{Ed25519PrivateKeyParameters, Ed25519PublicKeyParameters}
import org.healthlog.key.SigningKey
import scodec.bits.ByteVector

//noinspection ScalaWeakerAccess
object SignaturePipeline {

  val signPair: AsymmetricCipherKeyPair = SigningKey.generateKP()
  val senderPublicSigningKey: PublicSigningKey = SigningKey.convertToPublicSigningKey(signPair.getPublic.asInstanceOf[Ed25519PublicKeyParameters])
  val senderPrivateSigningKey: PrivateSigningKey = SigningKey.convertToPrivateSigningKey(signPair.getPrivate.asInstanceOf[Ed25519PrivateKeyParameters])

  /** convert SigningKey(s) to Ed25519KeyParameters */
  val ed25519PublicKey: Ed25519PublicKeyParameters = new Ed25519PublicKeyParameters(senderPublicSigningKey.bytes.toArray, 0)
  val ed25519PrivateKey: Ed25519PrivateKeyParameters = new Ed25519PrivateKeyParameters(senderPrivateSigningKey.bytes.toArray, 0)

  val msg: ByteVector = RandomByteVector.generateRandomByteVector(16).unsafeRunSync()
  val signature: Signature = SignatureBroker.sign(ed25519PrivateKey, msg)

  val signing: Ed25519Signing = Ed25519Signing.apply(
    (_, _) => { signature },
    (_, _, _) => { SignatureBroker.verify(ed25519PublicKey, msg, signature) }
  )
}