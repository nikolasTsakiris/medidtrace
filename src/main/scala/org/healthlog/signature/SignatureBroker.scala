package org.healthlog.signature

import com.ironcorelabs.recrypt.internal.Signature
import org.bouncycastle.crypto.params.{Ed25519PrivateKeyParameters, Ed25519PublicKeyParameters}
import org.bouncycastle.crypto.signers.Ed25519Signer
import scodec.bits.ByteVector

object SignatureBroker {

  /**
   * Signs a message using the provided private key.
   *
   * @param privateKey The Ed25519 private key used for signing.
   * @param message    The message to be signed as a `ByteVector`.
   * @return A `Signature` containing the generated signature.
   */
  def sign(privateKey: Ed25519PrivateKeyParameters, message: ByteVector): Signature = {
    val signer = new Ed25519Signer()
    signer.init(true, privateKey)
    signer.update(message.toArray, 0, message.length.toInt)
    val signatureBytes = signer.generateSignature()
    Signature(ByteVector(signatureBytes))
  }

  /**
   * Verifies a provided signature against a public key and message.
   *
   * @param publicKey The Ed25519 public key used for verification.
   * @param message   The original message as a `ByteVector`.
   * @param signature The signature to be verified.
   * @return `true` if the signature is valid, `false` otherwise.
   */
  def verify(publicKey: Ed25519PublicKeyParameters, message: ByteVector, signature: Signature): Boolean = {
    val signer = new Ed25519Signer()
    signer.init(false, publicKey)
    signer.update(message.toArray, 0, message.length.toInt)
    signer.verifySignature(signature.bytes.toArray)
  }
}