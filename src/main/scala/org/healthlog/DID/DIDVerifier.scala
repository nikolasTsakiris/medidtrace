package org.healthlog.DID

import scala.collection.mutable

// Define Verifier trait for shared functionality
sealed trait DIDVerifier {
  def name: String

  def DID: String
}
/*
case class Doctor(name: String, DID: String) extends DIDVerifier

case class HealthService(name: String, DID: String) extends DIDVerifier*/

object VerifierStorage {
  private val didDocuments: mutable.Map[String, DIDDocument] = mutable.Map.empty

  def storeDIDDocument(verifier: DIDVerifier, document: DIDDocument): Unit = {
    didDocuments += (verifier.DID -> document)
  }

  def getDIDDocument(did: String): Option[DIDDocument] = {
    didDocuments.get(did)
  }
}

object DIDVerifier {

  /**
   * Verifies data using a verifier's DID document and a signature.
   *
   * @param verifier  The verifier for whom the verification is performed.
   * @param data      The data to be verified.
   * @param signature The signature to be verified against the data.
   * @return `true` if the signature is valid, `false` if the signature is invalid or the verifier's DID document is not found.
   */
  def verify(): Unit = {}

  /**
   * Updates the org.healthlog.DID.DIDDocument for a verifier with the provided content.
   *
   * @param verifier  The verifier for whom the org.healthlog.DID.DIDDocument should be updated.
   * @param content   The JSON content to be added to the org.healthlog.DID.DIDDocument.
   * @param holderDID The holder's DID to retrieve the verifier's existing org.healthlog.DID.DIDDocument.
   * @return An option containing the updated org.healthlog.DID.DIDDocument if the update is successful, or None if the verifier's org.healthlog.DID.DIDDocument is not found.
   */
  def writeContent(verifier: DIDVerifier, content: String, holderDID: Holder): Option[DIDDocument] = {

    Holder.requestDID(holderDID).map { didDocument =>
      val updatedDIDDocument = didDocument.copy(
        content
      )

      VerifierStorage.storeDIDDocument(verifier, updatedDIDDocument)
      HolderStorage.tempStoreDIDDocument(holderDID, updatedDIDDocument)
      updatedDIDDocument
    }
  }
}
