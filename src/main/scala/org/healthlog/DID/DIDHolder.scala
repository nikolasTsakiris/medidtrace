package org.healthlog.DID

import org.healthlog

import scala.collection.mutable

case class Holder(name: String, DID: String)

object HolderStorage {
  private val didDocuments: mutable.Map[String, DIDDocument] = mutable.Map.empty

  def tempStoreDIDDocument(holder: Holder, document: DIDDocument): Unit = {
    didDocuments += (holder.DID -> document)
  }

  def getDIDDocument(did: String): Option[healthlog.DID.DIDDocument] = {
    didDocuments.get(did)
  }
}

object Holder {
  def requestDID(holder: Holder): Option[DIDDocument] = {
    HolderStorage.getDIDDocument(holder.DID)
  }
}