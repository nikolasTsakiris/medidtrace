package org.healthlog.DID

import io.circe._
import io.circe.generic.semiauto._

import java.util.UUID
import scala.util.Random

/** declaration of DID components */
case class Controller(id: String, `type`: String, name: String, MultibasePublicKey: String)

case class KeyAgreement(id: String, `type`: String, controller: String, publicKeyMultibase: String )

case class VerificationMethod(id: String, `type`: String, controller: String, MultibasePublicKey: String)

case class Authentication(`type`: String, MultibasePublicKey: String)

case class Service(id: String, `type`: String, serviceEndpoint: String)

case class DIDDocument(
                        id: String,
                        controller: Seq[Controller],
                        keyAgreement: Seq[KeyAgreement],
                        /*verificationMethod: Seq[VerificationMethod],
                        authentication: Seq[Authentication],
                        assertionMethod: Seq[String],*/
                        serviceEndpoints: List[Service],
                        content: String
                      )

object DIDDocument {

  implicit val verificationMethodEncoder: Encoder[VerificationMethod] = deriveEncoder[VerificationMethod]
  implicit val verificationMethodDecoder: Decoder[VerificationMethod] = deriveDecoder[VerificationMethod]
  implicit val keyAgreementMethodEncoder: Encoder[KeyAgreement] = deriveEncoder[KeyAgreement]
  implicit val keyAgreementMethodDecoder: Decoder[KeyAgreement] = deriveDecoder[KeyAgreement]
  implicit val authenticationEncoder: Encoder[Authentication] = deriveEncoder[Authentication]
  implicit val authenticationDecoder: Decoder[Authentication] = deriveDecoder[Authentication]
  implicit val controllerEncoder: Encoder[Controller] = deriveEncoder[Controller]
  implicit val controllerDecoder: Decoder[Controller] = deriveDecoder[Controller]
  implicit val serviceEncoder: Encoder[Service] = deriveEncoder[Service]
  implicit val serviceDecoder: Decoder[Service] = deriveDecoder[Service]
  implicit val DIDDocumentEncoder: Encoder[DIDDocument] = deriveEncoder[DIDDocument]
  implicit val DIDDocumentDecoder: Decoder[DIDDocument] = deriveDecoder[DIDDocument]

  /**
   * Generates a DID Document for a patient.
   *
   * @param ppk The public key for the patient.
   * @return A org.healthlog.DID.DIDDocument instance representing the patient.
   */
  def generateDIDDocument(pbk: String): DIDDocument = {
    val DIDMethod: String = "e_health"
    val uniqueID: String = UUID.randomUUID().toString

    /*val governmentController = Controller(
      s"did:$DIDMethod:$uniqueID#government",
      "GovernmentOrganization",
      "Government Agency Name",
      "5SbtwbUjjoALCAfF6gPAnHgbqb3qmmGwJiPh6YTj4Cv")*/

    val patientController = Controller(
      s"did:$DIDMethod:$uniqueID",
      "Person",
      Random.nextString(10),
      pbk)

    val keyAgreement = KeyAgreement(
      s"did:$DIDMethod:$uniqueID",
      "X25519KeyAgreementKey2019",
      s"did:$DIDMethod:$uniqueID",
      pbk)

    //TODO FIX PATIENT-KEY
    /*val authentication = Authentication("Ed25519SignatureAuthentication2018", s"#patient-key")

    val assertionMethods = Seq(s"#government-key", "5SbtwbUjjoALCAfF6gPAnHgbqb3qmmGwJiPh6YTj4Cv")*/
    val service1 = Service(s"did:$DIDMethod:$uniqueID#vcr1", "Hospital1", "https://example.com/vc1-endpoint")
    val service2 = Service(s"did:$DIDMethod:$uniqueID#vcr2", "Hospital2", "https://example.com/vc2-endpoint")
    val doc1 = Service(s"did:$DIDMethod:$uniqueID#doc1", "DoctorJoeDoe", "https://example.com/doc-endpoint3")
    val serviceList: List[Service] = List(service1, service2, doc1)
    val content = "-"

    DIDDocument(
      s"did:$DIDMethod:$uniqueID",
      Seq(patientController),
      Seq(keyAgreement),
      /*Seq(authentication),
      assertionMethods,
      Seq(governmentController, patientController),*/
      serviceList,
      content
    )
  }
}