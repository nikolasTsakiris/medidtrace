package org.healthlog.DID

import com.ironcorelabs.recrypt.{PrivateKey, PublicKey}
import org.healthlog.DID
import org.healthlog.kafka.TypeIssueResult
import org.healthlog.key.Asymmetric

import scala.util.Random

object Issuer {

  /**
   * Issue a DID with associated data and return a TypeIssueResult.
   *
   * @return A `TypeIssueResult` containing the core API, sender and receiver key pairs,
   *         patient DID document, patient DID ID, and a `Patient` instance.
   */
  def issue(): TypeIssueResult = {

    val (privk, pubk): (PrivateKey, PublicKey) = Asymmetric.generatePPK()
    val encodedPublicKey: String = Asymmetric.encodePublicKey(pubk)
    val DIDDocument: DIDDocument = DID.DIDDocument.generateDIDDocument(encodedPublicKey)
    val SubjectDID: String = DIDDocument.id
    val subject: Holder = Holder(name = Random.nextString(10) , DID = SubjectDID)

    TypeIssueResult(privk, pubk, DIDDocument, SubjectDID, subject)
  }
}
