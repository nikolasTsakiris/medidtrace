package org.healthlog.kafka

import com.ironcorelabs.recrypt.{PrivateKey, PublicKey}
import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecord, KafkaConsumer}
import org.apache.kafka.common.serialization.StringDeserializer
import org.healthlog.DID.{DIDDocument, Holder, Issuer}

import java.util.{Collections, Properties}

sealed trait ConsumerResult

case class TypeIssueResult(prvk: PrivateKey, pubk: PublicKey, didDocument: DIDDocument, didId: String, sub: Holder) extends ConsumerResult

case class TypeInvokeResult(trigger: String) extends ConsumerResult

//noinspection ScalaWeakerAccess
object Consumer {

  /**
   * Consume messages from a Kafka topic and process them based on their content.
   *
   * @return A `ConsumerResult` representing the outcome of processing the Kafka messages.
   */
  def consume(): ConsumerResult = {
    val topic = "DID-issuing-topic"
    val kafkaProps = new Properties()
    kafkaProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
    kafkaProps.put(ConsumerConfig.GROUP_ID_CONFIG, "DIDIssuerConsumer")
    kafkaProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer].getName)
    kafkaProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer].getName)

    val consumer = new KafkaConsumer[String, String](kafkaProps)
    consumer.subscribe(Collections.singletonList(topic))
    var result: ConsumerResult = null

    try {
      val timeout = java.time.Duration.ofSeconds(5) // Consume messages for:...
      val startTime = java.time.Instant.now()

      while (java.time.Instant.now().isBefore(startTime.plus(timeout)))  {
        val records = consumer.poll(java.time.Duration.ofSeconds(1))
        records.forEach { record: ConsumerRecord[String, String] =>
          println(s"Received message: ${record.value()}")

          record.value() match {
            case "ISSUE" =>
              result = typeIssue()
            case "InvokeProxyPipeline" =>
              result = typeINV("InvokeProxyPipeline")
            case "InvokeAccess" =>
              result = typeINV("InvokeAccess")
          }
        }
      }
    } finally {
      consumer.close()
    }
    result
  }

  /**
   * Process Type A message and generate a `TypeIssueResult`.
   *
   * @return A `TypeIssueResult` containing information related to Type A processing.
   */
  def typeIssue(): TypeIssueResult = {
    Issuer.issue()
  }

  /**
   * Process Type INV (Function INVocation) message and generate a `TypeInvokeResult`.
   *  * @return A `TypeInvokeResult` containing information related to Type INV processing.
   */
  def typeINV(trigger: String): TypeInvokeResult = {
    trigger match {
      case "InvokeProxyPipeline" =>
        TypeInvokeResult("InvokeProxyPipeline")
      case "InvokeAccess" =>
        TypeInvokeResult("InvokeAccess")
    }
  }
}