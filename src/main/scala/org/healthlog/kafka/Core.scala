package org.healthlog.kafka
import com.ironcorelabs.recrypt.CoreApi
import org.healthlog.signature.{RandomByteVector, SHA256, SignaturePipeline}

object Core {
  val coreApi: CoreApi = {
    new CoreApi(
      randomByteVector = RandomByteVector.generateRandomByteVector(16),
      sha256Impl = SHA256.sha256Hash,
      signing = SignaturePipeline.signing
    )
  }
}
