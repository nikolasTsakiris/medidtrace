package org.healthlog.kafka

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}
import org.apache.kafka.common.serialization.StringSerializer

import java.util.Properties

//noinspection ScalaWeakerAccess
object Producer {

  /**
   * Produce a message to a Kafka topic.
   *
   * @param msg The message to be produced.
   * @return No specific return value; the message is sent to the Kafka topic.
   */
  def produce(msg: String): Unit = {
    val topic = "DID-issuing-topic"
    val kafkaProps = new Properties()
    kafkaProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
    kafkaProps.put(ProducerConfig.CLIENT_ID_CONFIG, "DIDIssuerProducer")
    kafkaProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getName)
    kafkaProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getName)

    val producer = new KafkaProducer[String, String](kafkaProps)

    val record = new ProducerRecord[String, String](topic, null, msg)

    producer.send(record)

    producer.close()
  }
}