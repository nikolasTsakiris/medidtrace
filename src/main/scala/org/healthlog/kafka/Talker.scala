package org.healthlog.kafka

import com.ironcorelabs.recrypt.{PrivateKey, PublicKey}
import org.healthlog.DID.{DIDDocument, Holder}

object Talker {

  /**
   * Initiates the process of issuing a decentralized identifier (DID) and associated information.
   *
   * @return A tuple containing the private key, public key, DID document, DID, and subject holder.
   */
  def issueTalk(): (PrivateKey, PublicKey, DIDDocument, String, Holder) = {

    Producer.produce("ISSUE")
    val result: ConsumerResult = Consumer.consume()
    val (
      prvk: PrivateKey,
      pubk: PublicKey,
      doc: DIDDocument,
      did: String,
      subject: Holder
      ) = ResultHandler.processResult(result)

    (prvk, pubk, doc, did, subject)
  }
}