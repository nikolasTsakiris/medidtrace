package org.healthlog.kafka

import com.ironcorelabs.recrypt.{CoreApi, Plaintext, PrivateKey, PublicKey}
import org.healthlog.proxy.ProxyPipeline

object ResultHandler {

  /**
   * Process the given ConsumerResult and execute corresponding actions based on its type.
   *
   * @param result The ConsumerResult to process.
   * @param args   Additional arguments used for specific actions.
   * @return Any - The result of processing, depending on the executed action.
   */
  def processResult[T](result: ConsumerResult, args: T*): Any = {
    result match {

      case TypeIssueResult(privk, pubk, doc, did, sub) =>
        (privk, pubk, doc, did, sub)

      case TypeInvokeResult(trigger)
        if trigger == "InvokeProxyPipeline" =>
        ProxyPipeline.proxyJobs(
          args(0).asInstanceOf[CoreApi],
          args(1).asInstanceOf[Plaintext],
          args(2).asInstanceOf[PrivateKey],
          args(3).asInstanceOf[PublicKey],
          args(4).asInstanceOf[PublicKey]
        )

      /*case TypeInvokeResult(trigger)
        if trigger == "InvokeAccess" =>
        AccessController.control(args(0).asInstanceOf[String], args(1).asInstanceOf[Patient])*/
    }
  }

}