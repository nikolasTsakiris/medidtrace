package org.healthlog.key

import com.ironcorelabs.recrypt.{PrivateSigningKey, PublicSigningKey}
import org.bouncycastle.crypto.AsymmetricCipherKeyPair
import org.bouncycastle.crypto.generators.Ed25519KeyPairGenerator
import org.bouncycastle.crypto.params.{Ed25519KeyGenerationParameters, Ed25519PrivateKeyParameters, Ed25519PublicKeyParameters}
import scodec.bits.ByteVector

import java.security.SecureRandom

// noinspection ScalaWeakerAccess
object SigningKey {

  /**
   * Generates a new key pair for Ed25519 signing.
   *
   * @return An `AsymmetricCipherKeyPair` containing the generated key pair.
   */
  def generateKP(): AsymmetricCipherKeyPair = {
    val secureRandom = new SecureRandom()
    val keyGenParams = new Ed25519KeyGenerationParameters(secureRandom)
    val keyPairGen = new Ed25519KeyPairGenerator()
    keyPairGen.init(keyGenParams)
    keyPairGen.generateKeyPair()
  }

  /**
   * Converts an Ed25519PublicKeyParameters to a PublicSigningKey.
   *
   * @param publicKeyParams The Ed25519PublicKeyParameters to convert.
   * @return A PublicSigningKey representing the converted public key.
   */
  def convertToPublicSigningKey(publicKeyParams: Ed25519PublicKeyParameters): PublicSigningKey = {
    val publicKeyBytes: ByteVector = ByteVector(publicKeyParams.getEncoded)
    PublicSigningKey(publicKeyBytes)
  }

  /**
   * Converts an Ed25519PrivateKeyParameters to a PrivateSigningKey.
   *
   * @param privateKeyParams The Ed25519PrivateKeyParameters to convert.
   * @return A PrivateSigningKey representing the converted private key.
   */
  def convertToPrivateSigningKey(privateKeyParams: Ed25519PrivateKeyParameters): PrivateSigningKey = {
    val privateKeyBytes: ByteVector = ByteVector(privateKeyParams.getEncoded)
    PrivateSigningKey(privateKeyBytes)
  }
}