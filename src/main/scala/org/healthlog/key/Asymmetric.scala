package org.healthlog.key

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import com.ironcorelabs.recrypt.{PrivateKey, PublicKey}
import io.ipfs.multibase.Multibase
import org.healthlog.kafka.Core
import scodec.bits.ByteVector

object Asymmetric {

  /**
   * Generates a new pair of public and private keys.
   *
   * @return A tuple containing the generated private and public keys.
   */
  def generatePPK(): (PrivateKey, PublicKey) = {
    val keyPairIO: IO[(PrivateKey, PublicKey)] = Core.coreApi.generateKeyPair
    val keyPair: (PrivateKey, PublicKey) = keyPairIO.unsafeRunSync()
    keyPair
  }

  /**
   * Encodes a public key into a Multibase-encoded string using Base58BTC encoding.
   *
   * @param publicKey The public key to be encoded.
   * @return The Multibase-encoded string representing the public key.
   */
  def encodePublicKey(publicKey: PublicKey): String = {
    val x: ByteVector = publicKey.x
    val y: ByteVector = publicKey.y
    val xy: ByteVector = x ++ y
    val bytes: Array[Byte] = xy.toArray
    Multibase.encode(Multibase.Base.Base58BTC, bytes)
  }

  /**
   * Decodes a Multibase-encoded public key string into two ByteVector components.
   *
   * @param encodedPublicKey The Multibase-encoded public key string.
   * @return Either a tuple containing the decoded x and y ByteVectors or an error message if decoding fails.
   */
  def decodePublicKey(encodedPublicKey: String): Either[String, (ByteVector, ByteVector)] = {
    try {
      val decodedBytes = Multibase.decode(encodedPublicKey)
      val originalBytes = ByteVector(decodedBytes)
      val xBytes = originalBytes.take(32)
      val yBytes = originalBytes.drop(32)
      Right((xBytes, yBytes))
    } catch {
      case _: Exception => Left("Failed to decode public key")
    }
  }
}