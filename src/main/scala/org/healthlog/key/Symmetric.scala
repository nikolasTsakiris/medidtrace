package org.healthlog.key

import cats.effect.IO
import tsec.cipher.common.padding.NoPadding
import tsec.cipher.symmetric._
import tsec.cipher.symmetric.jca._
import tsec.cipher.symmetric.jca.primitive.JCAPrimitiveCipher

object Symmetric {

  /**
   * Encrypts the given message using AES256CTR encryption.
   *
   * @param msg      The message to be encrypted.
   * @param keyBytes The key bytes for encryption.
   * @return An IO containing the encrypted cipher text.
   */

  implicit private val ctrStrategy: IvGen[IO, AES256CTR] = AES256CTR.defaultIvStrategy[IO]
  implicit private val cachedInstance: JCAPrimitiveCipher[IO, AES256CTR, CTR, NoPadding] = AES256CTR.genEncryptor[IO]

  def symEncrypt(msg: String, keyBytes: Array[Byte]): IO[CipherText[AES256CTR]] = {

    val key: SecretKey[AES256CTR] = AES256CTR.unsafeBuildKey(keyBytes)

    val encrypted: IO[CipherText[AES256CTR]] = for {
      encrypted <- AES256CTR.encrypt[IO](PlainText(msg.getBytes), key)
    } yield encrypted

    encrypted
  }

  /**
   * Decrypts the given cipher text using AES256CTR decryption.
   *
   * @param cipher   The cipher text to be decrypted.
   * @param keyBytes The key bytes for decryption.
   * @return An IO containing the decrypted plain text.
   */
  def symDecrypt(cipher: CipherText[AES256CTR], keyBytes: Array[Byte]): IO[String] = {

    val key: SecretKey[AES256CTR] = AES256CTR.unsafeBuildKey(keyBytes)

    val decrypted: IO[String] = for {
      decrypted <- AES256CTR.decrypt[IO](cipher, key: SecretKey[AES256CTR])
    } yield decrypted.toUtf8String

    decrypted
  }
}