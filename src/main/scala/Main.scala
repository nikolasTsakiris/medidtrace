import cats.effect.IO
import cats.effect.unsafe.implicits.global
import com.ironcorelabs.recrypt.{CoreApi, EncryptedValue, Plaintext, PrivateKey, PublicKey}
import org.healthlog.DID.{DIDDocument, Holder, HolderStorage}
import org.healthlog.cassandra.DIDRegistry
import org.healthlog.kafka._
import org.healthlog.key.{Asymmetric, Symmetric}

/**
 * Main Application for Healthcare System
 * Allows users to interactively run various healthcare-related operations.
 */
object Main {

  def main(args: Array[String]): Unit = {
    runApplication()
  }

  def runApplication(): Unit = {
    val coreApi: CoreApi = Core.coreApi

    var patient: Holder = null
    var doctor: Holder = null
    var patientDID: String = null
    var doctorDID: String = null
    var patientDIDDocument: DIDDocument = null
    var doctorDIDDocument: DIDDocument = null
    var patientPrivateKey: PrivateKey = null
    var patientPublicKey: PublicKey = null
    var doctorPrivateKey: PrivateKey = null
    var doctorPublicKey: PublicKey = null

    var exitLoop = false
    while (!exitLoop) {
      println("Choose an option:")
      println("1. Start scheme with Patient-Doctor")
      println("2. Print values.")
      println("3. Patient Visits Doctor => Doctor Unlocks Data. Proceeding with proxy...")
      println("7. Truncate all Cassandra tables and start over.")
      println("8. Exit")

      print("Enter your choice: ")
      val choice = scala.io.StdIn.readLine()

      choice match {

        case "1" =>
          val resP = Talker.issueTalk()
          patientPrivateKey = resP._1
          patientPublicKey = resP._2
          patientDIDDocument = resP._3
          patientDID = resP._4
          patient = resP._5

          val resD = Talker.issueTalk()
          doctorPrivateKey = resP._1
          doctorPublicKey = resP._2
          doctorDIDDocument = resP._3
          doctorDID = resP._4
          doctor = resP._5

          HolderStorage.tempStoreDIDDocument(patient, patientDIDDocument)
          HolderStorage.tempStoreDIDDocument(doctor, doctorDIDDocument)
          DIDRegistry.addDID(patient, "patient")
          DIDRegistry.addDID(doctor, "doctor")
          DIDRegistry.addHealthcareData(patient)
          DIDRegistry.addHealthcareData(doctor)
          DIDRegistry.addAccessControl(patient, doctor)

          println("\nPatient-Doctor scheme initialized successfully\n")

        case "2" =>
          println("\nPrinting variable values:")
          println(s"patient: $patient")
          println(s"doctor: $doctor")
          println(s"patientDID: $patientDID")
          println(s"doctorDID: $doctorDID")
          println(s"patientDIDDocument: $patientDIDDocument")
          println(s"doctorDIDDocument: $doctorDIDDocument")
          println(s"patientPrivateKey: $patientPrivateKey")
          println(s"patientPublicKey: $patientPublicKey")
          println(s"doctorPrivateKey: $doctorPrivateKey")
          println(s"doctorPublicKey: $doctorPublicKey\n")

        case "3" =>

          val message: Plaintext = coreApi.generatePlaintext.unsafeRunSync()
          val symmetricKey: Array[Byte] = coreApi.deriveSymmetricKey(message).bytes.toArray
          val symmetricallyEncryptedData = Symmetric.symEncrypt(HolderStorage.getDIDDocument(patientDID).toString(), symmetricKey)

          Producer.produce("InvokeProxyPipeline")
          val result = Consumer.consume()

          val transformedValue = ResultHandler.processResult(
            result,
            coreApi,
            message,
            patientPrivateKey,
            patientPublicKey,
            doctorPublicKey
          )

          val decrypted: Either[String, Plaintext] = coreApi.decrypt(
            transformedValue.asInstanceOf[IO[EncryptedValue]].unsafeRunSync(), doctorPrivateKey)

          decrypted match {
            case Right(plaintext) =>
              println(s"Decryption succeeded: $plaintext")
              val symK: Array[Byte] = coreApi.deriveSymmetricKey(plaintext).bytes.toArray
              val deced = Symmetric.symDecrypt(symmetricallyEncryptedData.unsafeRunSync(), symK)
              println(s"symK is: ${symK.mkString}")
              println(s"deced is: ${deced.unsafeRunSync()}")
              DIDRegistry.addAuditTrail(doctor, patient, "READ")

            case Left(error) =>
              println(s"Decryption failed with error: $error")
          }

        case "7" =>
          try {
            DIDRegistry.truncateAll()
            println("\nDIDRegistry truncated successfully.\n")
          } catch {
            case ex: Exception =>
              println(s"Error truncating DIDRegistry: ${ex.getMessage}")
          }

        case "8" =>
          exitLoop = true
      }
    }
  }
}

