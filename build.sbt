ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / scalaVersion := "2.13.12"

resolvers += "Multiformats Maven Repository" at "https://maven.scijava.org/content/repositories/public/"
val tsecV = "0.4.0"
lazy val root = (project in file("."))
  .settings(
    name := "healthlog",
      libraryDependencies += "io.circe" %% "circe-core" % "0.14.0",
      libraryDependencies += "io.circe" %% "circe-generic" % "0.14.0",
      libraryDependencies += "io.circe" %% "circe-parser" % "0.14.0",
      libraryDependencies += "com.ironcorelabs" %% "recrypt-core" % "6.0.0",
      libraryDependencies += "org.bouncycastle" % "bcprov-jdk15on" % "1.68",
      libraryDependencies += "org.apache.kafka" %% "kafka" % "3.6.0",
      libraryDependencies ++= Seq(
        "ch.qos.logback" % "logback-classic" % "1.2.6",
        "ch.qos.logback" % "logback-core" % "1.2.6"
      ),
      libraryDependencies ++= Seq(
        "io.github.jmcardon" %% "tsec-common" % tsecV,
        "io.github.jmcardon" %% "tsec-password" % tsecV,
        "io.github.jmcardon" %% "tsec-cipher-jca" % tsecV,
        "io.github.jmcardon" %% "tsec-cipher-bouncy" % tsecV,
        "io.github.jmcardon" %% "tsec-mac" % tsecV,
        "io.github.jmcardon" %% "tsec-signatures" % tsecV,
        "io.github.jmcardon" %% "tsec-hash-jca" % tsecV,
        "io.github.jmcardon" %% "tsec-hash-bouncy" % tsecV,
        "io.github.jmcardon" %% "tsec-jwt-mac" % tsecV,
        "io.github.jmcardon" %% "tsec-jwt-sig" % tsecV,
        "io.github.jmcardon" %% "tsec-http4s" % tsecV
      ),
      libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.9" % "test",
      libraryDependencies += "com.datastax.oss" % "java-driver-core" % "4.9.0",
      libraryDependencies += "com.github.multiformats" % "java-multibase" % "1.1.1"
  )
