# MeDiDtrace
## Self-Sovereign Decentralized Identity (DID) System for Healthcare Services
### Overview
###### This project explores a user-centric implementation for managing healthcare data using decentralized identity systems. Additionally, it presents a control and monitoring model that empowers individuals to manage their own data while ensuring security, privacy, and control. It also provides notifications for third-party access. The research focuses on the integration of Decentralized Identifiers (DIDs), proxy re-encryption techniques, and distributed communication protocols to facilitate data access in the healthcare sector.

#### Note: The project description in Greek is provided below.

### Περίληψη
###### Η εργασία διερευνά μια υλοποίηση με επίκεντρο τον χρήστη, για τη διαχείριση των δεδομένων υγειονομικής περίθαλψης χρησιμοποιώντας αποκεντρωμένα συστήματα ταυτότητας. Επιπροσθέτως, παρουσιάζει ένα μοντέλο ελέγχου και παρακολούθησης που δίνει τη δυνατότητα στα άτομα να διαχειρίζονται τα δικά τους δεδομένα, διασφαλίζοντας παράλληλα την ασφάλεια, την ιδιωτικότητα και τον έλεγχο, και παρέχοντας ειδοποιήσεις για πρόσβαση από τρίτους. Η έρευνα επικεντρώνεται στην ενσωμάτωση Αποκεντρωμένων Αναγνωριστικών (DIDs), τεχνικές επανακρυπτογράφησης και σε διαμοιρασμένα πρωτόκολλα επικοινωνίας, με σκοπό τη διευκόλυνση της πρόσβασης δεδομένων στο τομέα της υγειονομικής περίθαλψης.